var metApp = angular.module("metlife", ['ui.router', 'ui.bootstrap']);
metApp.config(['$stateProvider', function($stateProvider) {

    $stateProvider
        .state('login', {
            url: "/login",
            templateUrl: "html/login/login.html",
            controller: 'loginController'
        })
        .state('home', {
            url: "/home",
            templateUrl: "html/home/home.html",
            controller: 'homeController'
        })
        .state('subhome', {
            url: "/subhome",
            templateUrl: "html/subhome/subhome.html",
            controller: 'subController'
        })
        .state('nav', {
            url: "/nav",
            templateUrl: "html/nav/nav.html",
            controller: 'navController'
        })
        .state('install', {
            url: "/install",
            templateUrl: "html/caseinstall/caseInstall.html",
            controller: 'installController'
        })
        .state("otherwise", {
            url: "*path",
            template: "",
            controller: ['$state', function($state) {
                $state.go('login')
            }]
        });
}]);
