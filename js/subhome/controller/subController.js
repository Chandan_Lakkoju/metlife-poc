metApp.controller('subController', function($scope, $http , $location,sharedProperties) {
    $http.get("data/database.json")
        .success(function(response) {
            $scope.relationships = response.relationship;
            $scope.myleads=response.myleads[sharedProperties.getIndex()];
        });
    $scope.homepage = function() {
        $location.path("/home");
    };
    $scope.logout = function() {
        $location.path("/login");
        sharedProperties.setUserName('');
        sharedProperties.setIndex('');
        sharedProperties.setQuotes([]);
    };
});
